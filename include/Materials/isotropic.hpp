#pragma once

#include "Materials/material.hpp"
#include "Textures/albedo.hpp"
#include "Textures/solid_colour.hpp"
#include "Textures/texture.hpp"
#include "Util/random.hpp"
#include <memory>

namespace Materials {

class Isotropic : public Material {
public:
  Isotropic(const Util::RGB<double> &a) : albedo{a} {}
  Isotropic(std::shared_ptr<Textures::Texture> a) : albedo{std::move(a)} {}

  Scatter_Record scatter(const Tracer::Ray &incident,
                         const Tracer::Contact_Record &rec) const override {
    // Just scatter in any ol' random direction
    const auto dir = Util::random_vector_in_unit_sphere();
    const auto att = Textures::visit_albedo(albedo, rec);
    const auto new_ray = Tracer::Ray{rec.contact_point, dir, incident.time()};
    return {Scattered_Ray{new_ray, att}, Util::black};
  }

private:
  Textures::Albedo albedo;
};

} // namespace Materials
