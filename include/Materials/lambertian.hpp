#pragma once

#include "Materials/material.hpp"
#include "Textures/albedo.hpp"
#include "Util/constants.hpp"
#include <memory>
#include <random>
#include <variant>

namespace Materials {

class Lambertian : public Material {
public:
  Lambertian(const Util::RGB<double> &a) : albedo{a} {}

  Lambertian(std::shared_ptr<Textures::Texture> a) : albedo{std::move(a)} {}

  Scatter_Record scatter(const Tracer::Ray &incident,
                         const Tracer::Contact_Record &rec) const override;

private:
  Textures::Albedo albedo;
};

} // namespace Materials
