#pragma once
#include "Materials/material.hpp"
#include "Util/rgb.hpp"

namespace Materials {

class Glass : public Material {
public:
  Glass(double ri) : refraction_index{ri} {}

  Scatter_Record scatter(const Tracer::Ray &incident,
                         const Tracer::Contact_Record &rec) const;

private:
  double reflectance(double cos, double ref_index) const;
  double refraction_index;
  Util::RGB<double> albedo{1., 1., 1.};
};

} // namespace Materials
