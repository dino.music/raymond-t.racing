#pragma once
#include "Materials/scatter_record.hpp"
#include "Tracer/contact_record.hpp"
#include "Tracer/ray.hpp"
#include "Util/rgb.hpp"

namespace Materials {

class Material {
public:
  virtual Scatter_Record scatter(const Tracer::Ray &incident,
                                 const Tracer::Contact_Record &rec) const = 0;
  virtual ~Material() = default;

protected:
  Geometry::Vec3 orient_normal(const Geometry::Vec3 &normal,
                               const Geometry::Vec3 &dir) const {
    return normal.dot_prod(dir) < 0. ? normal : -normal;
  }
};

} // namespace Materials