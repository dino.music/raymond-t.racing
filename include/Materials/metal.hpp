#pragma once
#include "Materials/material.hpp"
#include "Util/rgb.hpp"

namespace Materials {

class Metal : public Material {
public:
  Metal(const Util::RGB<double> &a, double f = 0.) : albedo{a}, fuzz{f} {}

  Scatter_Record scatter(const Tracer::Ray &incident,
                         const Tracer::Contact_Record &rec) const override;

private:
  Util::RGB<double> albedo;
  double fuzz;
};

} // namespace Materials
