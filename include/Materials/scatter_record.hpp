#pragma once

#include "Tracer/ray.hpp"
#include "Util/rgb.hpp"

namespace Materials {

struct Scattered_Ray {
  Tracer::Ray ray;
  Util::RGB<double> attenuation;
};

struct Scatter_Record {
  std::optional<Scattered_Ray> scatter;
  Util::RGB<double> emitted;
};

} // namespace Materials
