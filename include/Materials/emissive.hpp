#pragma once

#include "Materials/material.hpp"
#include "Textures/albedo.hpp"
#include <memory>

namespace Materials {

class Emissive : public Material {
public:
  Emissive(std::shared_ptr<Textures::Texture> e) : emit{std::move(e)} {}
  Emissive(const Util::RGB<double> &c) : emit{c} {}

  Scatter_Record scatter(const Tracer::Ray &incident,
                         const Tracer::Contact_Record &rec) const override {
    return {std::nullopt, Textures::visit_albedo(emit, rec)};
  }

private:
  Textures::Albedo emit;
};

} // namespace Materials
