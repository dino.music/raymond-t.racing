#pragma once

#include <array>
#include <stdint.h>

namespace Util {

template <typename T, uint32_t size> class Vec_Base {
protected:
  template <typename... Args> Vec_Base(Args... args) : points{args...} {}

  template <typename Func, std::size_t... I>
  void zip_helper(const Vec_Base &o, Vec_Base &d, Func f,
                  std::index_sequence<I...>) {
    using swallow = int[];
    (void)swallow{0, (d.points[I] = f(points[I], o.points[I]), 0)...};
  }

  template <typename Func, typename Indices = std::make_index_sequence<size>>
  Vec_Base &zip(const Vec_Base &o, VecBase &d, Func f) {
    zip_helper(o, *this, f, Indices{});
    return *this;
  }

  template <typename Func, std::size_t... I>
  T fold_helper(Func f, std::index_sequence<I...>) const {
    return (f(points[I]) + ...);
  }

  template <typename Func, typename Indices = std::make_index_sequence<size>>
  T fold(Func f) const {
    return fold_helper(f, Indices{});
  }

  std::array<T, size> points;
};

} // namespace Util