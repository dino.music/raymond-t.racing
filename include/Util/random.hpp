#pragma once

#include <Geometry/vec3.hpp>
#include <random>

namespace Util {

extern thread_local std::default_random_engine random_engine;
extern thread_local std::uniform_real_distribution<double> unit_sphere_dist;
extern thread_local std::uniform_real_distribution<double> dist;

Geometry::Vec3 random_vector_on_unit_sphere();
Geometry::Vec3 random_vector_in_unit_sphere();
Geometry::Vec3 random_vector_in_unit_circle();
Geometry::Vec3 random_vector_on_hemisphere(const double e = 0.);
double rando_double();
double rando_double(double min, double max);

} // namespace Util
