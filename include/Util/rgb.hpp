#pragma once
#include <algorithm>
#include <array>
#include <stdint.h>
#include <type_traits>

namespace Util {

template <typename T,
          typename =
              typename std::enable_if<std::is_arithmetic<T>::value, T>::type>
class RGB {
public:
  RGB() = default;
  RGB(T r, T g, T b) : sub_pix{r, g, b} {}

  T r() const { return sub_pix[0]; }
  T g() const { return sub_pix[1]; }
  T b() const { return sub_pix[2]; }

  RGB &r(T r) { return (sub_pix[0] = r, *this); }
  RGB &g(T g) { return (sub_pix[1] = g, *this); }
  RGB &b(T b) { return (sub_pix[2] = b, *this); }

  RGB operator*(T s) const {
    return {sub_pix[0] * s, sub_pix[1] * s, sub_pix[2] * s};
  }

  RGB operator/(T s) const { return *(this) * (static_cast<T>(1) / s); }

  RGB operator*(const RGB &o) const {
    return {sub_pix[0] * o.sub_pix[0], sub_pix[1] * o.sub_pix[1],
            sub_pix[2] * o.sub_pix[2]};
  }

  RGB operator+(const RGB &o) const {
    return {sub_pix[0] + o.sub_pix[0], sub_pix[1] + o.sub_pix[1],
            sub_pix[2] + o.sub_pix[2]};
  }

  void operator+=(const RGB &o) {
    sub_pix[0] += o.sub_pix[0];
    sub_pix[1] += o.sub_pix[1];
    sub_pix[2] += o.sub_pix[2];
  }

  RGB &operator*=(const RGB &o) {
    sub_pix[0] *= o.sub_pix[0];
    sub_pix[1] *= o.sub_pix[1];
    sub_pix[2] *= o.sub_pix[2];
    return *this;
  }

  void operator*=(T s) {
    sub_pix[0] *= s;
    sub_pix[1] *= s;
    sub_pix[2] *= s;
  }

  template <typename Func> RGB &gamma_correct(Func f) {
    sub_pix[0] = f(sub_pix[0]);
    sub_pix[1] = f(sub_pix[1]);
    sub_pix[2] = f(sub_pix[2]);
    return *this;
  }

  std::string to_string() const {
    using namespace std::literals::string_literals;
    std::string s;
    s.reserve(15);
    s += "<"s + std::to_string(sub_pix[0]) + "," + std::to_string(sub_pix[1]) +
         "," + std::to_string(sub_pix[2]) + ">";
    return s;
  };

private:
  std::array<T, 3> sub_pix;
};

template <typename T> auto make_rgb(T r, T g, T b) { return RGB<T>(r, g, b); }

template <typename T, typename U>
Util::RGB<U> convert_rgb(const Util::RGB<T> &rgb, U base) {
  return Util::make_rgb(static_cast<U>(base * rgb.r()),
                        static_cast<U>(base * rgb.g()),
                        static_cast<U>(base * rgb.b()));
}

template <typename T>
Util::RGB<T> clamp_rgb(const Util::RGB<T> &rgb, T min, T max) {
  return Util::make_rgb(std::clamp(rgb.r(), min, max),
                        std::clamp(rgb.g(), min, max),
                        std::clamp(rgb.b(), min, max));
}

inline const Util::RGB<double> black{0., 0., 0.};

} // namespace Util