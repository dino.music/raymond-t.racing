#pragma once

#include "Util/rgb.hpp"
#include <limits>

namespace Util {

inline const double infinity = std::numeric_limits<double>::infinity();
inline const double pi = 3.1415926535897932385;
inline const double inv_pi = 1. / pi;

} // namespace Util
