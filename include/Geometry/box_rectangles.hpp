#pragma once

#include "Geometry/axis_aligned_rectangle.hpp"
#include "Tracer/traceable_list.hpp"

namespace Geometry {

class Box_Rectangles : public Tracer::Traceable {
public:
  Box_Rectangles(const Geometry::Point3 &l, const Geometry::Point3 &u,
                 std::shared_ptr<Materials::Material> m)
      : lower{l}, upper{u}, material{std::move(m)} {
    rects.insert(std::make_shared<Geometry::XY_Rectangle>(
        l.x(), u.x(), l.y(), u.y(), l.z(), material));
    rects.insert(std::make_shared<Geometry::XY_Rectangle>(
        l.x(), u.x(), l.y(), u.y(), u.z(), material));
    rects.insert(std::make_shared<Geometry::XZ_Rectangle>(
        l.x(), u.x(), l.z(), u.z(), l.y(), material));
    rects.insert(std::make_shared<Geometry::XZ_Rectangle>(
        l.x(), u.x(), l.z(), u.z(), u.y(), material));
    rects.insert(std::make_shared<Geometry::YZ_Rectangle>(
        l.y(), u.y(), l.z(), u.z(), l.x(), material));
    rects.insert(std::make_shared<Geometry::YZ_Rectangle>(
        l.y(), u.y(), l.z(), u.z(), u.x(), material));
  }

  std::optional<Tracer::Contact_Material>
  contact(const Tracer::Ray &ray, double t_min, double t_max) const override {
    return rects.contact(ray, t_min, t_max);
  }

  virtual std::optional<Tracer::Bounding_Box>
  bounding_box(double t0, double t1) const override {
    return Tracer::Bounding_Box{lower, upper};
  }

private:
  Geometry::Point3 lower, upper;
  Tracer::Traceable_List rects;
  std::shared_ptr<Materials::Material> material;
};

} // namespace Geometry
