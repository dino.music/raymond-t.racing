#pragma once

namespace Geometry {

enum class Axis { X = 0, Y, Z };

} // namespace Geometry
