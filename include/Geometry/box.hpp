#pragma once

#include "Geometry/axis_aligned_rectangle.hpp"
#include "Tracer/traceable.hpp"
#include "boost/math/special_functions/sign.hpp"
#include <array>
#include <utility>

namespace Geometry {

class Box : public Tracer::Traceable {
public:
  Box(const Geometry::Point3 l, const Geometry::Point3 u,
      std::shared_ptr<Materials::Material> m)
      : lower{l}, upper{u}, material{m}, centre{(l + u) * 0.5},
        invRadius{Geometry::Vec3{2., 2., 2.} / (u - l)} {}

  std::optional<Tracer::Contact_Material>
  contact(const Tracer::Ray &ray, double t_min, double t_max) const override;

  std::optional<Tracer::Bounding_Box> bounding_box(double t0,
                                                   double t1) const override {
    return Tracer::Bounding_Box{lower, upper};
  }

private:
  Geometry::Point3 lower, upper;
  std::shared_ptr<Materials::Material> material;
  Geometry::Point3 centre;
  Geometry::Vec3 invRadius;
};

} // namespace Geometry
