#pragma once
// #include "vec_base.hpp"
#include "Geometry/point3.hpp"
#include <array>
#include <math.h>
#include <stdexcept>
#include <string>

namespace Geometry {

class Vec3 { //: public Vec_Base<double, 3> {
public:
  Vec3() = default;
  Vec3(double x, double y, double z, double e = 0.) : point{x, y, z, e} {}
  Vec3(const Point3 &p) : point{p} {}
  Vec3(const Vec3 &) = default;

  double x() const { return point.x(); }
  double y() const { return point.y(); }
  double z() const { return point.z(); }

  Vec3 &x(double x) { return (point.x(x), *this); }
  Vec3 &y(double y) { return (point.y(y), *this); }
  Vec3 &z(double z) { return (point.z(z), *this); }

  double operator[](size_t i) const { return point[i]; }
  double &operator[](size_t i) { return point[i]; }

  Vec3 operator+(const Vec3 &o) const { return Vec3(point + o.point); }

  Vec3 operator-(const Vec3 &o) const { return Vec3(point - o.point); }

  Vec3 operator-() const { return {-point.x(), -point.y(), -point.z()}; }

  Vec3 operator*(double s) const { return Vec3(point * s); }

  Vec3 &operator+=(Vec3 &o) {
    point += o.point;
    return *this;
  }

  Vec3 &operator-=(Vec3 &o) {
    point -= o.point;
    return *this;
  }

  Vec3 &operator*=(double s) {
    point *= s;
    return *this;
  }

  Vec3 operator*(const Vec3 &o) const {
    return {point[0] * o.point[0], point[1] * o.point[1],
            point[2] * o.point[2]};
  }

  Vec3 operator/(const Vec3 &o) const {
    return {point[0] / o.point[0], point[1] / o.point[1],
            point[2] / o.point[2]};
  }

  double dot_prod(const Vec3 &o) const {
    return point.x() * o.point.x() + point.y() * o.point.y() +
           point.z() * o.point.z();
  }

  Vec3 cross_prod(const Vec3 &o) const {
    return Vec3(point.y() * o.point.z() - point.z() * o.point.y(),
                point.z() * o.point.x() - point.x() * o.point.z(),
                point.x() * o.point.y() - point.y() * o.point.x());
  }
  double modulus_squared() const {
    return point.x() * point.x() + point.y() * point.y() +
           point.z() * point.z();
  }
  double modulus() const { return sqrt(modulus_squared()); }

  Vec3 normalize() const {
    const auto mod = modulus();
    if (mod == 0.)
      throw std::invalid_argument("Attempted to normalize a zero vector");
    return operator*(1 / mod);
  }

  bool epsilon_zero(const double e) const {
    return (fabs(point.x()) < e) && (fabs(point.y()) < e) &&
           (fabs(point.z()) < e);
  }

  Vec3 reflect(const Vec3 &normal) const {
    return *this - normal * dot_prod(normal) * 2;
  }

  std::string to_string() const {
    using namespace std::literals::string_literals;
    std::string s;
    s.reserve(15);
    s += "["s + std::to_string(point.x()) + "," + std::to_string(point.y()) +
         "," + std::to_string(point.z()) + " " + std::to_string(point[3]) + "]";
    return s;
  };

  friend Vec3 operator*(double s, const Vec3 &vec) { return vec * s; }
  friend Point3 operator+(const Point3 &p, const Vec3 &v) {
    return p + v.point;
  }

private:
  Point3 point{0., 0., 0.};
};

inline Vec3 expand_vec(const Vec3 &vec, double e) {
  return Vec3{vec[0], vec[1], vec[2], e};
}

} // namespace Geometry