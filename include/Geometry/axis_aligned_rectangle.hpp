#pragma once

#include "Geometry/axis.hpp"
#include "Materials/material.hpp"
#include "Tracer/traceable.hpp"

namespace Geometry {

constexpr Axis axes_lookup[3][2] = {
    {Axis::Y, Axis::Z}, {Axis::X, Axis::Z}, {Axis::X, Axis::Y}};

constexpr int select_axis(Axis fixed, int selector) {
  return static_cast<int>(axes_lookup[static_cast<int>(fixed)][selector]);
}

template <int f_axis, int h_axis, int v_axis>
class Axis_Aligned_Rectangle_Imp : public Tracer::Traceable {
public:
  Axis_Aligned_Rectangle_Imp(double l, double r, double d, double u, double f,
                             std::shared_ptr<Materials::Material> m)
      : left{l}, right{r}, down{d}, up{u}, fixed{f}, mat{std::move(m)} {}

  std::optional<Tracer::Contact_Material>
  contact(const Tracer::Ray &ray, double t_min, double t_max) const override {
    const auto t = (fixed - ray.origin()[f_axis]) / ray.direction()[f_axis];
    if (t < t_min || t > t_max)
      return std::nullopt;
    const auto h = ray.origin()[h_axis] + ray.direction()[h_axis] * t;
    const auto v = ray.origin()[v_axis] + ray.direction()[v_axis] * t;
    if (h < left || h > right || v < down || v > up)
      return std::nullopt;
    Geometry::Point3 contact_point;
    contact_point[f_axis] = fixed;
    contact_point[h_axis] = h;
    contact_point[v_axis] = v;
    Geometry::Vec3 surface_normal{0., 0., 0.};
    surface_normal[f_axis] = 1.;
    const auto texture_u = (h - left) / (right - left);
    const auto texture_v = (v - down) / (up - down);
    return Tracer::Contact_Material{
        {contact_point, surface_normal, t, texture_u, texture_v, t}, mat};
  }

  std::optional<Tracer::Bounding_Box> bounding_box(double t0,
                                                   double t1) const override {
    Geometry::Point3 lp, rp;
    lp[f_axis] = fixed - 0.0001;
    lp[h_axis] = left;
    lp[v_axis] = down;

    rp[f_axis] = fixed + 0.0001;
    rp[h_axis] = right;
    rp[v_axis] = up;
    return Tracer::Bounding_Box{lp, rp};
  }

private:
  double left, right;
  double down, up;
  double fixed;
  std::shared_ptr<Materials::Material> mat;
};

template <Axis a>
using Axis_Aligned_Rectangle =
    Axis_Aligned_Rectangle_Imp<static_cast<int>(a), select_axis(a, 0),
                               select_axis(a, 1)>;

using XY_Rectangle = Axis_Aligned_Rectangle<Axis::Z>;
using XZ_Rectangle = Axis_Aligned_Rectangle<Axis::Y>;
using YZ_Rectangle = Axis_Aligned_Rectangle<Axis::X>;

} // namespace Geometry
