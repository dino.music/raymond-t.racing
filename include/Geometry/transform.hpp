#pragma once

#include "Geometry/mat4.hpp"
#include "Tracer/traceable.hpp"

namespace Geometry {

struct Trans_Mtx {
  Trans_Mtx(const Geometry::Mat4 &t) : trans{t}, inv_trans{*t.inverse()} {}
  Geometry::Mat4 trans;
  Geometry::Mat4 inv_trans;
};

class Transform : public Tracer::Traceable {
public:
  Transform(std::shared_ptr<Tracer::Traceable> o, std::shared_ptr<Trans_Mtx> &t)
      : obj{std::move(o)}, mtx{std::move(t)} {}

  Transform(std::shared_ptr<Tracer::Traceable> o, const Mat4 &t)
      : obj{std::move(o)}, mtx{std::make_shared<Trans_Mtx>(t)} {}

  std::optional<Tracer::Contact_Material>
  contact(const Tracer::Ray &ray, double t_min, double t_max) const override;
  std::optional<Tracer::Bounding_Box> bounding_box(double t0,
                                                   double t1) const override;

private:
  std::shared_ptr<Tracer::Traceable> obj;
  std::shared_ptr<Trans_Mtx> mtx;
}; // namespace Geometry::Transform

} // namespace Geometry
