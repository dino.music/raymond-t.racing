#pragma once

#include "Geometry/vec3.hpp"
#include "Util/constants.hpp"
#include "point3.hpp"
#include <Geometry/axis.hpp>
#include <array>
#include <cmath>
#include <optional>

namespace Geometry {

class Mat4 {
public:
  Mat4 operator*(const Mat4 &o) const;
  std::optional<Mat4> inverse() const;
  double operator()(size_t i, size_t j) const { return coeff[i][j]; }

  template <Geometry::Axis a> static Mat4 make_rotation_matrix(double angle);

  static Mat4 make_translation_matrix(double x, double y, double z);

private:
  bool invert_matrix(const double m[16], double invOut[16]) const;

  std::array<std::array<double, 4>, 4> coeff{
      std::array<double, 4>{0., 0., 0., 0.},
      std::array<double, 4>{0., 0., 0., 0.},
      std::array<double, 4>{0., 0., 0., 0.},
      std::array<double, 4>{0., 0., 0., 0.}};
};

template <Geometry::Axis a> Mat4 Mat4::make_rotation_matrix(double angle) {
  static_assert(a == Geometry::Axis::X || a == Geometry::Axis::Y ||
                a == Geometry::Axis::Z);
  Mat4 temp;
  const auto rad_angle = angle * Util::pi / 180;
  const auto cos_angle = std::cos(rad_angle);
  const auto sin_angle = std::sin(rad_angle);
  if constexpr (a == Geometry::Axis::X) {
    temp.coeff[0][0] = 1.;
    temp.coeff[1][1] = cos_angle;
    temp.coeff[1][2] = -sin_angle;
    temp.coeff[2][1] = sin_angle;
    temp.coeff[2][2] = cos_angle;
    temp.coeff[3][3] = 1.;
  } else if (a == Geometry::Axis::Y) {
    temp.coeff[0][0] = cos_angle;
    temp.coeff[0][2] = sin_angle;
    temp.coeff[1][1] = 1.;
    temp.coeff[2][0] = -sin_angle;
    temp.coeff[2][2] = cos_angle;
    temp.coeff[3][3] = 1.;
  } else if (a == Geometry::Axis::Z) {
    temp.coeff[0][0] = cos_angle;
    temp.coeff[0][1] = -sin_angle;
    temp.coeff[1][0] = sin_angle;
    temp.coeff[1][1] = cos_angle;
    temp.coeff[2][2] = 1.;
    temp.coeff[3][3] = 1.;
  }
  return temp;
}

Point3 transform_point(const Mat4 &m, const Point3 &p);
Vec3 transform_vector(const Mat4 &m, const Vec3 &v);
Vec3 transform_normal(const Mat4 &m, const Vec3 &n);

} // namespace Geometry
