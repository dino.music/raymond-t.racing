#pragma once
#include "Geometry/point3.hpp"
#include "Materials/material.hpp"
#include "Tracer/traceable.hpp"
#include <memory>

namespace Geometry {

class Sphere : public Tracer::Traceable {
public:
  Sphere(const Geometry::Point3 &c, double r,
         std::shared_ptr<Materials::Material> m)
      : centre{c}, radius{r}, material{std::move(m)} {}
  Sphere(double cx, double cy, double cz, double r,
         std::shared_ptr<Materials::Material> m)
      : centre{cx, cy, cz}, radius{r}, material{std::move(m)} {}

  std::optional<Tracer::Contact_Material>
  contact(const Tracer::Ray &ray, double t_min, double t_max) const override;

  std::optional<Tracer::Bounding_Box> bounding_box(double t0,
                                                   double t1) const override;

private:
  std::pair<double, double> texture_coords(const Geometry::Point3 &p) const;

  Geometry::Point3 centre{0., 0., 0.};
  double radius{1.};
  std::shared_ptr<Materials::Material> material;
};

} // namespace Geometry
