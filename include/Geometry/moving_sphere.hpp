#pragma once

#include "Geometry/point3.hpp"
#include "Materials/material.hpp"
#include "Tracer/traceable.hpp"
#include <memory>

namespace Geometry {
class Moving_Sphere : public Tracer::Traceable {
public:
  Moving_Sphere(const Geometry::Point3 &startc, double startt,
                const Geometry::Point3 &destc, double destt, double radius,
                std::shared_ptr<Materials::Material> m)
      : cs{startc}, cd{destc}, r{radius}, ts{startt}, td{destt}, material{
                                                                     std::move(
                                                                         m)} {}
  std::optional<Tracer::Contact_Material>
  contact(const Tracer::Ray &ray, double t_min, double t_max) const override;

  std::optional<Tracer::Bounding_Box> bounding_box(double t0,
                                                   double t1) const override;

private:
  Geometry::Point3 centre(double t) const;
  std::pair<double, double> texture_coords(const Geometry::Point3 &p) const;

  Geometry::Point3 cs;
  Geometry::Point3 cd;
  double r;
  double ts, td;
  std::shared_ptr<Materials::Material> material;
};

} // namespace Geometry
