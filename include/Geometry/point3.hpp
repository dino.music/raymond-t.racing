#pragma once
#include <array>
#include <cmath>

namespace Geometry {
class Point3 {
public:
  Point3() = default;
  Point3(double x, double y, double z, double e = 0.) : coords{x, y, z, e} {}
  Point3(const Point3 &) = default;

  double x() const { return coords[0]; }
  double y() const { return coords[1]; }
  double z() const { return coords[2]; }

  Point3 &x(double x) { return (coords[0] = x, *this); }
  Point3 &y(double y) { return (coords[1] = y, *this); }
  Point3 &z(double z) { return (coords[2] = z, *this); }

  Point3 operator+(const Point3 &o) const {
    return Point3(coords[0] + o.coords[0], coords[1] + o.coords[1],
                  coords[2] + o.coords[2]);
  }

  Point3 operator-(const Point3 &o) const {
    return Point3(coords[0] - o.coords[0], coords[1] - o.coords[1],
                  coords[2] - o.coords[2]);
  }

  Point3 operator-() const { return {-coords[0], -coords[1], -coords[2]}; }

  double operator[](size_t i) const { return coords[i]; }
  double &operator[](size_t i) { return coords[i]; }

  Point3 operator*(double s) const {
    return Point3(coords[0] * s, coords[1] * s, coords[2] * s);
  }

  Point3 &operator+=(const Point3 &o) {
    coords[0] += o.coords[0];
    coords[1] += o.coords[1];
    coords[2] += o.coords[2];
    return *this;
  }

  Point3 &operator-=(const Point3 &o) {
    coords[0] -= o.coords[0];
    coords[1] -= o.coords[1];
    coords[2] -= o.coords[2];
    return *this;
  }

  Point3 &operator*=(double s) {
    coords[0] *= s;
    coords[1] *= s;
    coords[2] *= s;
    return *this;
  }

  bool operator==(const Point3 &o) const {
    return coords[0] == o.coords[0] && coords[1] == o.coords[1] &&
           coords[2] == o.coords[2];
  }

  std::string to_string() const {
    using namespace std::literals::string_literals;
    std::string s;
    s.reserve(15);
    s += "("s + std::to_string(coords[0]) + "," + std::to_string(coords[1]) +
         "," + std::to_string(coords[2]) + " " + std::to_string(coords[3]) +
         ")";
    return s;
  };

  double modulus_squared() const {
    return coords[0] * coords[0] + coords[1] * coords[1] +
           coords[2] * coords[2];
  }

  double modulus() const { return sqrt(modulus_squared()); }

  Point3 normalize() const {
    const auto mod = modulus();
    if (mod == 0.)
      throw std::invalid_argument("Attempted to normalize a zero vector");
    return operator*(1 / mod);
  }

private:
  std::array<double, 4> coords{0., 0., 0., 1.};
};

inline Point3 expand_point(const Point3 &p, double e) {
  return Point3{p[0], p[1], p[2], e};
}

} // namespace Geometry