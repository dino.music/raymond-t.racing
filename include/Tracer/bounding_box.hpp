#pragma once

#include "Geometry/point3.hpp"
#include "Tracer/ray.hpp"
#include <memory>

namespace Tracer {
class Bounding_Box {
public:
  Bounding_Box() = default;
  Bounding_Box(const Geometry::Point3 &l, const Geometry::Point3 &u)
      : lower_{l}, upper_{u} {}

  bool contact(const Tracer::Ray &ray, double t_min, double t_max) const;

  Bounding_Box encompassing_box(const Bounding_Box &o) const;

  const Geometry::Point3 &lower() const { return lower_; }
  const Geometry::Point3 &upper() const { return upper_; }

private:
  Geometry::Point3 lower_;
  Geometry::Point3 upper_;
};

} // namespace Tracer
