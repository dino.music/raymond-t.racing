#pragma once
#include "Tracer/traceable.hpp"
#include "Tracer/traceable_list.hpp"
#include <memory>

namespace Tracer {

class BVH : public Traceable {
public:
  using it = std::vector<std::shared_ptr<Traceable>>::iterator;

  BVH() = default;

  BVH(std::vector<std::shared_ptr<Tracer::Traceable>> objects, double t0,
      double t1);

  BVH(const Tracer::Traceable_List &list, double t0, double t1)
      : BVH(list.objects(), t0, t1) {}

  std::optional<Contact_Material> contact(const Ray &ray, double t_min,
                                          double t_max) const override;

  std::optional<Bounding_Box> bounding_box(double t0,
                                           double t1) const override {
    return box;
  }

private:
  std::shared_ptr<Traceable> left;
  std::shared_ptr<Traceable> right;
  Bounding_Box box;
};

} // namespace Tracer
