#pragma once

#include "Materials/isotropic.hpp"
#include "Tracer/traceable.hpp"
#include "Util/constants.hpp"
#include "Util/random.hpp"

namespace Tracer {

template <typename T> class Constant_Medium : public Traceable {
public:
  Constant_Medium(T &&o, double d, const Util::RGB<double> &a)
      : obj{std::forward<T>(o)}, neg_inv_opt_density{-1. / d},
        mat{std::make_shared<Materials::Isotropic>(a)} {}
  template <typename... Args>
  Constant_Medium(double d, const Util::RGB<double> &a, Args &&... args)
      : obj{std::forward<Args>(args)...}, neg_inv_opt_density{-1. / d},
        mat{std::make_shared<Materials::Isotropic>(a)} {}

  std::optional<Contact_Material> contact(const Ray &ray, double t_min,
                                          double t_max) const override {
    auto rec = obj.contact(ray, -Util::infinity, Util::infinity);
    if (!rec)
      return std::nullopt;
    double t0 = std::max(rec->record.t, t_min);
    double t1 = std::min(rec->record.t1, t_max);

    if (t0 >= t1)
      return std::nullopt;
    t0 = std::max(t0, 0.);

    const auto rl = ray.direction().modulus();
    const auto dist = (t1 - t0) * rl;
    const auto hit_dist = neg_inv_opt_density * std::log(Util::rando_double());
    if (hit_dist > dist)
      return std::nullopt;
    const auto t = t0 + hit_dist / rl;
    const auto cp = ray(t);
    return Contact_Material{{cp, Geometry::Vec3{0., 1., 0.}, t0, 0., 0., t1},
                            mat};
  }

  std::optional<Bounding_Box> bounding_box(double t0,
                                           double t1) const override {
    return obj.bounding_box(t0, t1);
  }

private:
  T obj;
  double neg_inv_opt_density;
  std::shared_ptr<Materials::Isotropic> mat;
};

} // namespace Tracer
