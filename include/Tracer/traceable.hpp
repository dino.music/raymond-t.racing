#pragma once
#include "Materials/material.hpp"
#include "Tracer/bounding_box.hpp"
#include "Tracer/contact_record.hpp"
#include "Tracer/ray.hpp"
#include <memory>
#include <optional>

namespace Tracer {

struct Contact_Material {
  Contact_Record record;
  std::shared_ptr<Materials::Material> material;
};

class Traceable {
public:
  virtual std::optional<Contact_Material> contact(const Ray &ray, double t_min,
                                                  double t_max) const = 0;
  virtual std::optional<Bounding_Box> bounding_box(double t0,
                                                   double t1) const = 0;
  virtual ~Traceable() = default;
};

} // namespace Tracer
