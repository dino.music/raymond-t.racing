#pragma once

#include "Tracer/traceable.hpp"
#include <memory>
#include <vector>

namespace Tracer {

class Traceable_List : public Traceable {
public:
  void insert(std::shared_ptr<Traceable> obj) {
    objs.push_back(std::move(obj));
  }

  const std::vector<std::shared_ptr<Traceable>> &objects() const {
    return objs;
  }

  std::optional<Contact_Material> contact(const Ray &ray, double t_min,
                                          double t_max) const override;
  std::optional<Bounding_Box> bounding_box(double t0, double t1) const override;

private:
  std::vector<std::shared_ptr<Traceable>> objs;
};

} // namespace Tracer
