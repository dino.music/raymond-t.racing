#pragma once

#include "Geometry/point3.hpp"
#include "Geometry/vec3.hpp"

namespace Tracer {

class Ray {
public:
  Ray() = default;
  Ray(const Geometry::Point3 &o, const Geometry::Vec3 &d, double t)
      : origin_{o}, direction_{d},
        inv_direction_{1. / d[0], 1. / d[1], 1. / d[2], 0.}, time_{t} {}

  const Geometry::Point3 &origin() const { return origin_; }
  const Geometry::Vec3 &direction() const { return direction_; }
  const Geometry::Vec3 &inv_direction() const { return inv_direction_; }
  double time() const { return time_; }

  // P(t) = A + tb
  // A - point origin of the ray
  // b - direction vector of the ray
  // t - by adjusting t we move point P(t) along the ray
  Geometry::Point3 at(double t) const { return origin_ + t * direction_; }
  Geometry::Point3 operator()(double t) const { return at(t); }

private:
  Geometry::Point3 origin_;
  Geometry::Vec3 direction_;
  Geometry::Vec3 inv_direction_;
  double time_{0.};
};

} // namespace Tracer
