#pragma once

#include "Geometry/point3.hpp"
#include "Geometry/vec3.hpp"

namespace Tracer {

struct Contact_Record {
  Geometry::Point3 contact_point;
  Geometry::Vec3 surface_normal;
  double t;
  double texture_u, texture_v;
  double t1;
};

} // namespace Tracer
