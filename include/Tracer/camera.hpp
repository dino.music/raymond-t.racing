#pragma once

#include "Geometry/point3.hpp"
#include "Geometry/vec3.hpp"
#include "Tracer/ray.hpp"
#include "Util/constants.hpp"
#include "Util/random.hpp"
#include <cmath>

namespace Tracer {

class Camera {
public:
  Camera(const Geometry::Point3 &o, const Geometry::Point3 &t,
         const Geometry::Vec3 &up, double aspect_ratio, double vertical_fov,
         double aperture, double focal_distance, double shutter_tmin,
         double shutter_tmax)
      : shutter_time_min{shutter_tmin}, shutter_time_max{shutter_tmax} {
    const auto theta = vertical_fov * Util::pi / 180.;
    const auto h = tan(theta / 2);
    const auto viewport_height = 2.0 * h;
    const auto viewport_width = aspect_ratio * viewport_height;
    w = Geometry::Vec3(o - t).normalize();
    u = up.cross_prod(w).normalize();
    v = w.cross_prod(u);

    origin = o;
    hor = u * viewport_width * focal_distance;
    ver = v * viewport_height * focal_distance;
    ll =
        Geometry::Vec3(origin) - (hor * 0.5) - (ver * 0.5) - w * focal_distance;
    lens_radius = aperture / 2.;
  }

  Tracer::Ray yeet_ray(const double s, const double t) const {
    Geometry::Vec3 rd = Util::random_vector_in_unit_circle() * lens_radius;
    Geometry::Vec3 offset = u * rd.x() + v * rd.y();
    auto direction = ll + hor * s + ver * t - origin - offset;
    const auto time = Util::rando_double(shutter_time_min, shutter_time_max);
    return {origin + offset, direction, time};
  }

private:
  Geometry::Point3 origin;
  Geometry::Vec3 hor;
  Geometry::Vec3 ver;
  Geometry::Vec3 ll;
  Geometry::Vec3 u, v, w;
  double lens_radius;
  double shutter_time_min, shutter_time_max;
};

} // namespace Tracer
