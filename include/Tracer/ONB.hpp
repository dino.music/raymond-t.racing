#pragma once

#include "Geometry/vec3.hpp"

namespace Tracer {

struct ONB {
  ONB(const Geometry::Vec3 &n) : w{n} {
    v = (std::abs(w.x()) > std::abs(w.y()))
            ? Geometry::Vec3{-w.z(), 0., w.x()}.normalize()
            : Geometry::Vec3{0., w.z(), -w.y()}.normalize();
    u = w.cross_prod(v);
  }

  Geometry::Vec3 local(const Geometry::Vec3 &a) const {
    return u * a.x() + v * a.y() + w * a.z();
  }

  Geometry::Vec3 u, v, w;
};

} // namespace Tracer
