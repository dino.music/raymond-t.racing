#pragma once

#include "Textures/texture.hpp"

namespace Textures {

class Solid_Colour : public Texture {
public:
  Solid_Colour(const Util::RGB<double> c) : colour{c} {}
  Solid_Colour(double r, double g, double b) : colour{r, g, b} {}

  Util::RGB<double> attenuation(double, double,
                                const ::Geometry::Point3 &) const override {
    return colour;
  }

private:
  Util::RGB<double> colour;
};

} // namespace Textures
