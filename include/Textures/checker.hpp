#pragma once

#include "Textures/albedo.hpp"
#include <cmath>
#include <memory>

namespace Textures {

class Checker : public Texture {
public:
  Checker(std::shared_ptr<Texture> p, std::shared_ptr<Texture> n, double s)
      : pos{std::move(p)}, neg{std::move(n)}, scale{s} {}
  Checker(const Util::RGB<double> &pc, const Util::RGB<double> &nc, double s)
      : pos{pc}, neg{nc}, scale{s} {}

  Util::RGB<double> attenuation(double u, double v,
                                const Geometry::Point3 &p) const {
    const auto sine =
        sin(p.x() * scale) * sin(p.y() * scale) * sin(p.z() * scale);
    return sine < 0. ? Textures::visit_albedo(neg, u, v, p)
                     : Textures::visit_albedo(pos, u, v, p);
  }

private:
  Textures::Albedo pos, neg;
  double scale;
};

} // namespace Textures
