#pragma once

#include "Textures/texture.hpp"
#include "Util/rgb.hpp"
#include <memory>
#include <variant>

namespace Textures {

using Albedo = std::variant<Util::RGB<double>, std::shared_ptr<Texture>>;

inline Util::RGB<double> visit_albedo(const Albedo &albedo, double u, double v,
                                      const Geometry::Point3 &p) {
  const auto f = [u, v, &p](auto &&arg) {
    using T = std::decay_t<decltype(arg)>;
    if constexpr (std::is_same_v<T, Util::RGB<double>>)
      return arg;
    else
      return arg->attenuation(u, v, p);
  };
  return std::visit(f, albedo);
}

inline Util::RGB<double> visit_albedo(const Albedo &albedo,
                                      const Tracer::Contact_Record &rec) {
  return visit_albedo(albedo, rec.texture_u, rec.texture_v, rec.contact_point);
}

} // namespace Textures
