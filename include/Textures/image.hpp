#pragma once

#include <Textures/texture.hpp>
#include <memory>
#include <string_view>

namespace Textures {

class Image : public Texture {
public:
  Image(std::string_view file_name);

  Util::RGB<double> attenuation(double u, double v,
                                const Geometry::Point3 &p) const override;

private:
  std::unique_ptr<unsigned char[]> data;
  int w, h;
  int bytes_per_scanline;
};

} // namespace Textures
