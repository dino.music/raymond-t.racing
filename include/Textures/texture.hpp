#pragma once

#include "Geometry/point3.hpp"
#include "Util/rgb.hpp"

namespace Textures {

class Texture {
public:
  virtual Util::RGB<double> attenuation(double u, double v,
                                        const Geometry::Point3 &p) const = 0;
  virtual ~Texture() = default;
};

} // namespace Textures
