#include "Geometry/vec3.hpp"
#include <gtest/gtest.h>
#include <math.h>
#include <stdexcept>

using namespace testing;
using namespace Geometry;

class Vec3Test : public Test {
protected:
  Vec3 i{1., 0., 0.};
  Vec3 j{0., 1., 0.};
  Vec3 k{0., 0., 1.};
  Vec3 zero_vector{0., 0., 0.};

  static void compare_vectors(const Vec3 &actual, const Vec3 &expected) {
    EXPECT_EQ(actual.x(), expected.x());
    EXPECT_EQ(actual.y(), expected.y());
    EXPECT_EQ(actual.z(), expected.z());
  }
};

TEST_F(Vec3Test, ExpectsDefaultConstructedVectorToBeZeroVector) {
  Vec3 vec;
  compare_vectors(vec, zero_vector);
}

TEST_F(Vec3Test, ExpectsModulusOfDefaultConstructedVectorToBeZero) {
  Vec3 vec;
  EXPECT_EQ(vec.modulus(), 0.);
}

TEST_F(Vec3Test, ExpectsNormalizationOfZeroVectorToThrow) {
  Vec3 vec;
  EXPECT_THROW(vec.normalize(), std::invalid_argument);
}

TEST_F(Vec3Test, ExpectsModulusOfUnitVectorToBeOne) {
  EXPECT_EQ(i.modulus(), 1.);
  EXPECT_EQ(j.modulus(), 1.);
  EXPECT_EQ(k.modulus(), 1.);
}

TEST_F(Vec3Test, ExpectsModulusOfNormalizedVectorToBeOne) {
  const Vec3 vec(1., 2., 3.);
  const auto n = vec.normalize();
  EXPECT_EQ(n.modulus(), 1.);
}

TEST_F(Vec3Test, ExpectsScalingOfVectorWithNegativeOneToInvertIt) {
  Vec3 vec1{1., 2., 3.}, vec2{vec1 * -1.};
  compare_vectors(vec2, {-vec1.x(), -vec1.y(), -vec1.z()});
}

TEST_F(Vec3Test, ExpectsCrossProductWithZeroVectorToBeZeroVector) {
  const Vec3 vec1, vec2(3., 4., 5.);
  compare_vectors(vec1.cross_prod(vec2), zero_vector);
  compare_vectors(vec2.cross_prod(vec1), zero_vector);
  compare_vectors(vec1.cross_prod(vec1), zero_vector);
}

TEST_F(Vec3Test, ExpectsCrossProductOfVectorWithItselfToBeZeroVector) {
  const Vec3 vec(20., 13.4, 18.6234);
  compare_vectors(vec.cross_prod(vec), zero_vector);
}

TEST_F(Vec3Test, ExpectsCrossProductOfParallelVectorsToBeZeroVector) {
  const Vec3 vec1(1., 1., 1.), vec2(2., 2., 2.);
  compare_vectors(vec1.cross_prod(vec2), zero_vector);
}

TEST_F(Vec3Test, ExpectsCrossProductOfAntiParallelVectorsToBeZeroVector) {
  const Vec3 vecs[] = {{1., 0., 0.}, {0., 1., 0.}, {0., 0., 1.}};
  const Vec3 inverted_vecs[] = {{-1., 0., 0.}, {0, -1., 0.}, {0., 0., -1.}};
  for (auto i = 0u; i < 3; ++i) {
    compare_vectors(vecs[i].cross_prod(inverted_vecs[i]), {0., 0., 0.});
  }
}

TEST_F(Vec3Test, ExpectsCrossProductToBeAnticommutative) {
  const Vec3 vec1(-2.34, 18.679, -1.), vec2(1.18, 295.345654, -8000.);
  compare_vectors(vec1.cross_prod(vec2), (vec2.cross_prod(vec1)) * -1.);
}

TEST_F(Vec3Test, ExpectsCrossProductToBeDistributiveOverAddition) {
  compare_vectors(i.cross_prod(j + k), i.cross_prod(j) + i.cross_prod(k));
}

TEST_F(Vec3Test, ExpectsCrossVectorToBeCompatibleWithScalarMultiplication) {
  const double s = -1.;
  const auto r1 = (i * s).cross_prod(j), r2 = i.cross_prod(j * s),
             r3 = i.cross_prod(j) * s;
  compare_vectors(r1, r2);
  compare_vectors(r1, r3);
  compare_vectors(r2, r3);
}

TEST_F(Vec3Test, ExpectsCrossProductToSatisfyJacobyIdentity) {
  const auto res = i.cross_prod(j.cross_prod(k)) +
                   j.cross_prod(k.cross_prod(i)) +
                   k.cross_prod(i.cross_prod(j));
  compare_vectors(res, {0., 0., 0.});
}

TEST_F(Vec3Test, ExpectsDotProductToBeCommutative) {
  EXPECT_EQ(i.dot_prod(j), j.dot_prod(i));
}

TEST_F(Vec3Test, ExpectsDotProductOfOrthogonalVectorToBeZero) {
  EXPECT_EQ(i.dot_prod(j), 0.);
  EXPECT_EQ(i.dot_prod(k), 0.);
  EXPECT_EQ(j.dot_prod(k), 0.);
}

TEST_F(Vec3Test, ExpectsDotProductOfParallelVectorsToBeMultipleOfModulus) {
  Vec3 i2 = i * 2, i3 = i * 3;
  EXPECT_EQ(i2.dot_prod(i3), i2.modulus() * i3.modulus());
}

TEST_F(Vec3Test, ExpectsDotProductOfVectorWithItselfToEqualSquareOfModulus) {
  Vec3 i3 = i * 3;
  EXPECT_EQ(i3.dot_prod(i3), pow(i3.modulus(), 2));
}

TEST_F(Vec3Test, ExpectsDotProductToBeDistributiveOverVectorAddition) {
  EXPECT_EQ(i.dot_prod(j + k), i.dot_prod(j) + i.dot_prod(k));
}

TEST_F(Vec3Test, ExpectsDotProductToBeBilinear) {
  const double c = 3.;
  EXPECT_EQ(i.dot_prod(j * c + k), i.dot_prod(j) * c + i.dot_prod(k));
}