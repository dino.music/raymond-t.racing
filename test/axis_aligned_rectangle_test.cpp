#include "Geometry/axis_aligned_rectangle.hpp"
#include "Materials/material.hpp"
#include "Util/constants.hpp"
#include "gtest/gtest.h"

using namespace testing;
using namespace Util;
using namespace Geometry;
using namespace Tracer;
using namespace Materials;

class MockMaterial : public Materials::Material {
  Scatter_Record scatter(const Tracer::Ray &,
                         const Tracer::Contact_Record &) const override {
    return {std::nullopt, Util::black};
  }
};

class AARectTest : public Test {
protected:
  std::shared_ptr<Material> mat{std::make_shared<MockMaterial>()};
  void compare_points(const Point3 &actual, const Point3 &expected) {
    EXPECT_EQ(actual.x(), expected.x());
    EXPECT_EQ(actual.y(), expected.y());
    EXPECT_EQ(actual.z(), expected.z());
  }
  void compare_vectors(const Vec3 &actual, const Vec3 &expected) {
    EXPECT_EQ(actual.x(), expected.x());
    EXPECT_EQ(actual.y(), expected.y());
    EXPECT_EQ(actual.z(), expected.z());
  }
};

TEST_F(AARectTest, ExpectsBoundingBoxOfXYRectangleToBeCorrect) {
  XY_Rectangle rect{0., 5., 0., 10., 5., mat};
  const auto bb = *rect.bounding_box(0., 0.);
  compare_points(bb.lower(), {0., 0., 5. - 0.0001});
  compare_points(bb.upper(), {5., 10., 5. + 0.0001});
}

TEST_F(AARectTest, ExpectsBoundingBoxOfXZRectangleToBeCorrect) {
  XZ_Rectangle rect{0., 5., 0., 10., 5., mat};
  const auto bb = *rect.bounding_box(0., 0.);
  compare_points(bb.lower(), {0., 5. - 0.0001, 0.});
  compare_points(bb.upper(), {5., 5. + 0.0001, 10.});
}

TEST_F(AARectTest, ExpectsBoundingBoxOfYZRectangleToBeCorrect) {
  YZ_Rectangle rect{0., 5., 0., 10., 5., mat};
  const auto bb = *rect.bounding_box(0., 0.);
  compare_points(bb.lower(), {5. - 0.0001, 0., 0.});
  compare_points(bb.upper(), {5. + 0.0001, 5., 10.});
}

TEST_F(AARectTest, ExpectsRayParallelToXYRectangleToMiss) {
  XY_Rectangle rect{0., 5., 0., 10., 5., mat};
  Ray ray{{1., 1., 3.}, {10., 10., 3.}, 0.};
  const auto record = rect.contact(ray, 0.001, Util::infinity);
  EXPECT_EQ(record, std::nullopt);
}

TEST_F(AARectTest, ExpectsRayParallelToXZRectangleToMiss) {
  XZ_Rectangle rect{0., 5., 0., 10., 5., mat};
  Ray ray{{1., 3., 1.}, {10., 3., 10.}, 0.};
  const auto record = rect.contact(ray, 0.001, Util::infinity);
  EXPECT_EQ(record, std::nullopt);
}

TEST_F(AARectTest, ExpectsRayParallelToYZRectangleToMiss) {
  YZ_Rectangle rect{0., 5., 0., 10., 5., mat};
  Ray ray{{3., 1., 1.}, {3., 10., 10.}, 0.};
  const auto record = rect.contact(ray, 0.001, Util::infinity);
  EXPECT_EQ(record, std::nullopt);
}

TEST_F(AARectTest, ExpectsXYRectangleToGenerateCorrectContactData) {
  XY_Rectangle rect{-5., 5., -10., 10., 5., mat};
  Ray ray{{-1., -1., 0.}, {2., 2., 10}, 0.};
  const auto material_rec = rect.contact(ray, 0.001, Util::infinity);
  EXPECT_TRUE(material_rec.has_value());
  const auto contact_rec = material_rec->record;
  compare_points(contact_rec.contact_point, {0., 0., 5.});
  compare_vectors(contact_rec.surface_normal, {0., 0., 1.});
}

TEST_F(AARectTest, ExpectsXZRectangleToGenerateCorrectContactData) {
  XZ_Rectangle rect{-5., 5., -10., 10., 5., mat};
  Ray ray{{-1., 0., -1.}, {2., 10., 2.}, 0.};
  const auto material_rec = rect.contact(ray, 0.001, Util::infinity);
  EXPECT_TRUE(material_rec.has_value());
  const auto contact_rec = material_rec->record;
  compare_points(contact_rec.contact_point, {0., 5., 0.});
  compare_vectors(contact_rec.surface_normal, {0., 1., 0.});
}

TEST_F(AARectTest, ExpectsYZRectangleToGenerateCorrectContactData) {
  YZ_Rectangle rect{-5., 5., -10., 10., 5., mat};
  Ray ray{{0., -1., -1.}, {10., 2., 2.}, 0.};
  const auto material_rec = rect.contact(ray, 0.001, Util::infinity);
  EXPECT_TRUE(material_rec.has_value());
  const auto contact_rec = material_rec->record;
  compare_points(contact_rec.contact_point, {5., 0., 0.});
  compare_vectors(contact_rec.surface_normal, {1., 0., 0.});
}
