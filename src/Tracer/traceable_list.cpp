#include "Tracer/traceable_list.hpp"
#include "Util/constants.hpp"

namespace Tracer {

std::optional<Contact_Material>
Traceable_List::contact(const Ray &ray, double t_min, double t_max) const {
  std::optional<Contact_Material> ret;
  for (const auto &o : objs) {
    const auto contact_material = o->contact(ray, t_min, t_max);
    if (contact_material.has_value()) {
      ret = std::move(contact_material);
      t_max = ret->record.t;
    }
  }
  return ret;
}

std::optional<Bounding_Box> Traceable_List::bounding_box(double t0,
                                                         double t1) const {
  using namespace Util;
  auto box_acc = Bounding_Box{{infinity, infinity, infinity},
                              {-infinity, -infinity, -infinity}};
  for (const auto &o : objs) {
    const auto bb = o->bounding_box(t0, t1);
    if (!bb.has_value())
      return std::nullopt;
    box_acc = box_acc.encompassing_box(*bb);
  }
  return box_acc;
  // if (objs.empty())
  //   return std::nullopt;
  // bool first_box = true;
  // std::optional<Bounding_Box> temp_box;
  // Bounding_Box output_box;
  // for (const auto &o : objs) {
  //   temp_box = o->bounding_box(t0, t1);
  //   if (!temp_box.has_value())
  //     return std::nullopt;
  //   output_box = first_box ? *temp_box :
  //   temp_box->encompassing_box(output_box); first_box = false;
  // }
  // return output_box;
}

} // namespace Tracer
