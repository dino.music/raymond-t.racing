#include "Tracer/bvh.hpp"
#include "Util/random.hpp"
#include <iterator>

namespace Tracer {

BVH::BVH(std::vector<std::shared_ptr<Traceable>> objects, double t0,
         double t1) {
  const auto axis = static_cast<int>(Util::rando_double(0, 3));
  const auto comp_pred = [axis](const auto &l, const auto &r) {
    const auto boxl = l->bounding_box(0., 0.), boxr = r->bounding_box(0., 0.);
    if (!boxl.has_value() || !boxr.has_value())
      throw "Sike idiot";
    return boxl->lower()[axis] < boxr->lower()[axis];
  };
  const auto obj_count = objects.size();

  if (obj_count == 1) {
    left = right = objects[0];
  } else if (obj_count == 2) {
    left = objects[0];
    right = objects[1];
    if (!comp_pred(left, right))
      std::swap(left, right);
  } else {
    std::sort(objects.begin(), objects.end(), comp_pred);
    const auto mid = obj_count / 2;
    std::vector<std::shared_ptr<Traceable>> objs_right(
        std::move_iterator(objects.begin() + mid),
        std::move_iterator(objects.end())),
        objs_left(std::move_iterator(objects.begin()),
                  std::move_iterator(objects.begin() + mid));
    left = std::make_shared<BVH>(std::move(objs_left), t0, t1);
    right = std::make_shared<BVH>(std::move(objs_right), t0, t1);
  }
  const auto box_left = left->bounding_box(t0, t1);
  const auto box_right = right->bounding_box(t0, t1);
  if (!box_left.has_value() || !box_right.has_value())
    throw "Yeet";
  box = box_left->encompassing_box(*box_right);
}

std::optional<Contact_Material> BVH::contact(const Ray &ray, double t_min,
                                             double t_max) const {
  if (!box.contact(ray, t_min, t_max))
    return std::nullopt;
  const auto lcontact = left->contact(ray, t_min, t_max);
  const auto rcontact = right->contact(
      ray, t_min, lcontact.has_value() ? lcontact->record.t : t_max);
  if (!lcontact.has_value() && !rcontact.has_value())
    return std::nullopt;
  return rcontact.value_or(*lcontact);
}

} // namespace Tracer
