#include "Tracer/bounding_box.hpp"
#include <algorithm>

namespace Tracer {

bool Bounding_Box::contact(const Tracer::Ray &ray, double t_min,
                           double t_max) const {
  for (int i = 0; i < 3; ++i) {
    const auto d = ray.inv_direction()[i];
    auto t0 = (lower_[i] - ray.origin()[i]) * d;
    auto t1 = (upper_[i] - ray.origin()[i]) * d;
    if (d < 0.)
      std::swap(t0, t1);
    t_min = std::max(t0, t_min);
    t_max = std::min(t1, t_max);
    if (t_max <= t_min)
      return false;
  }
  return true;
}

Bounding_Box Bounding_Box::encompassing_box(const Bounding_Box &o) const {
  const auto l = Geometry::Point3{fmin(lower_.x(), o.lower_.x()),
                                  fmin(lower_.y(), o.lower_.y()),
                                  fmin(lower_.z(), o.lower_.z())};

  const auto u = Geometry::Point3{fmax(upper_.x(), o.upper_.x()),
                                  fmax(upper_.y(), o.upper_.y()),
                                  fmax(upper_.z(), o.upper_.z())};
  return {l, u};
}

} // namespace Tracer
