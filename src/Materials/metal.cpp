#include "Materials/metal.hpp"
#include "Util/random.hpp"

namespace Materials {

Scatter_Record Metal::scatter(const Tracer::Ray &incident,
                              const Tracer::Contact_Record &rec) const {
  // Performs regular old reflection
  // If the incoming vector is I, and the surface normal vector is N, we can get
  // the reflected vector R in the following way
  // I = Ix + Iy
  // Iy = (N dot I)*N
  // Ix = I - Iy = I - (N dot I)*N
  // R = Rx + Ry
  // Rx = Ix = I - (N dot I)*N
  // Ry = -Iy = -(N dot I)*N
  // R = I - (N dot I)*N - (N dot I)*N
  // R = I - 2*N*(N dot I)

  const auto surface_normal =
      orient_normal(rec.surface_normal, incident.direction());
  const auto dir = incident.direction().normalize().reflect(surface_normal) +
                   Util::random_vector_on_unit_sphere() * fuzz;
  // If the reflected ray is reflected at an angle >90 degrees
  if (dir.dot_prod(surface_normal) < 0.)
    return {std::nullopt, Util::black};
  const auto ray = Tracer::Ray(rec.contact_point, dir, incident.time());
  return {Scattered_Ray{ray, albedo}, Util::black};
}

} // namespace Materials
