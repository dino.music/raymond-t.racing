#include "Materials/lambertian.hpp"
#include "Tracer/ONB.hpp"
#include "Util/random.hpp"

namespace Materials {

Scatter_Record Lambertian::scatter(const Tracer::Ray &incident,
                                   const Tracer::Contact_Record &rec) const {
  Tracer::ONB coord{orient_normal(rec.surface_normal, incident.direction())};
  auto dir = coord.local(Util::random_vector_on_hemisphere());
  const auto ray = Tracer::Ray{rec.contact_point, dir, incident.time()};
  const auto att = Textures::visit_albedo(albedo, rec);
  return Scatter_Record{Scattered_Ray{ray, att}, Util::black};
}

} // namespace Materials
