#include "Materials/glass.hpp"
#include "Util/random.hpp"
#include <algorithm>

namespace Materials {

Scatter_Record Glass::scatter(const Tracer::Ray &incident,
                              const Tracer::Contact_Record &rec) const {
  // Refraction is described by Snells's law
  // eta * sin(theta) = eta' * sin(theta')
  // Where eta and theta refer to the incident ray, and eta' and theta' refer to
  // the refracted ray
  // To determine the direction of the refracted ray we have to figure out
  // theta'
  // Assume all vectors below are normalized
  // The refracted ray R' can be written in terms of it's x and y components as
  // R' = R'x + R'y, as can the incident ray R = Rx + Ry
  // |Ry| = |R|*cos(theta), |Rx| = |R|*sin(theta)
  // |R'y| = |R'|*cos(theta'), |R'x| = |R'|*sin(theta')
  // |R'x|/|Rx| = (|R'|*sin(theta'))/(|R|*sin(theta))
  // Given that the vectors are normalized this leads to
  // |R'x| = |Rx| * sin(theta')/sin(theta) = |Rx| * eta/eta'
  // R'x and Rx are parallel so the equvialence that stands for the moduli
  // stands for the vectors too
  // R'x = Rx * eta/eta' (substitue Rx = R - Ry)
  // Ry is parallel to the normal(but opposite in direction) so it can be
  // expessed as Ry = -N * |R| * cos(theta) = -N * cost(theta)
  // R'x = eta/eta'*(R + N*cos(theta))
  // By Pithagora's law |R'x|^2 + |R'y|^2 = |R'|^2, (|R'|^2 = 1)
  // |R'y| = sqrt(1 - |R'x|^2)
  // Just like Ry, R'y is parallel but opposite in direction to the normal so it
  // is given as
  // R'y = -N*sqrt(1 - |R'x|^2)
  // We can obtain cos(theta) from R dot N = - |R|*|N|*cos(theta)
  //|R| = 1, |N| = 1 => cos(theta) = -R dot N
  // So finally we get to
  // R'x = eta/eta'*(R - N*(R dot N))
  // R'y = -N*sqrt(1 - |R'x|^2)
  // R' = R'x + R'y
  const auto front_face =
      incident.direction().dot_prod(rec.surface_normal) < 0.;
  const auto rr = front_face ? (1. / refraction_index) : refraction_index;
  const auto surface_normal =
      front_face ? rec.surface_normal : -rec.surface_normal;

  const auto unit_incident = incident.direction().normalize();
  const auto cos_theta = std::min(-surface_normal.dot_prod(unit_incident), 1.0);
  const auto sin_theta = sqrt(1. - cos_theta * cos_theta);

  // If the incident ray is coming in at an angle higher than what is called the
  // critical angle, refraction will not occur. Instead, total internal
  // reflection takes place.
  // Looking at Snell's law we can deduce the following
  // eta * sin(theta) = eta' * sin(theta')
  // sin(theta') = eta/eta' * sin(theta)
  // Given that sin(theta') <= 1
  // eta/eta' * sin(theta) <= 1
  // Or if theta > arcsin(eta'/eta)
  // No solution can exist.
  const auto ref = reflectance(cos_theta, rr);
  const auto rando = Util::rando_double();
  Geometry::Vec3 direction;
  if ((rr * sin_theta > 1.0) || (ref > rando)) {
    // Reflect
    direction = unit_incident.reflect(surface_normal);
  } else {
    // Refract
    const auto refracted_x = (unit_incident + surface_normal * cos_theta) * rr;
    const auto refracted_y =
        surface_normal * (sqrt(fabs(1. - refracted_x.modulus_squared())));
    direction = refracted_x - refracted_y;
  }

  const auto ray = Tracer::Ray(rec.contact_point, direction, incident.time());
  return {Scattered_Ray{ray, albedo}, Util::black};
}

// The reflectance of glass changes with incident angle.
// The variability in reflectance is approximated using Schlick's approximation
double Glass::reflectance(double cos, double ref_index) const {
  const auto r = pow((1. - ref_index) / (1. + ref_index), 2.);
  return r + (1 - r) * pow((1 - cos), 5);
}

} // namespace Materials
