#include "Geometry/axis_aligned_rectangle.hpp"
#include "Geometry/box.hpp"
#include "Geometry/moving_sphere.hpp"
#include "Geometry/point3.hpp"
#include "Geometry/sphere.hpp"
#include "Geometry/transform.hpp"
#include "Geometry/vec3.hpp"
#include "Materials/emissive.hpp"
#include "Materials/glass.hpp"
#include "Materials/lambertian.hpp"
#include "Materials/metal.hpp"
#include "Textures/checker.hpp"
#include "Textures/image.hpp"
#include "Tracer/bvh.hpp"
#include "Tracer/camera.hpp"
#include "Tracer/constant_medium.hpp"
#include "Tracer/ray.hpp"
#include "Tracer/traceable_list.hpp"
#include "Util/constants.hpp"
#include "Util/rgb.hpp"
#include "boost/asio.hpp"
#include <algorithm>
#include <atomic>
#include <cstdlib>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <map>
#include <math.h>
#include <random>
#include <thread>
#include <variant>
#include <vector>

struct Scene_Data {
  int samples_per_pixel;
  double aspect_ratio;
  int img_w;
  int recursion_depth;
  Geometry::Point3 camera_origin;
  Geometry::Point3 camera_lookat;
  Geometry::Vec3 up;
  double focal_distance;
  double vfov;
  double aperture;
  Util::RGB<double> bg_clr;
  Tracer::BVH scene;
  double start_time;
  double end_time;
};

Scene_Data random_scene();
Scene_Data cornell_box();
Scene_Data cornell_box_smoke();
Scene_Data final_scene();

Util::RGB<double> trace(const Tracer::Traceable &, const Tracer::Ray &,
                        const Util::RGB<double>, int);

std::atomic<int> lines_complete{0};

std::optional<Scene_Data> get_scene(int i) {
  static std::map<int, Scene_Data (*)()> funcs{{0, random_scene},
                                               {1, cornell_box},
                                               {2, cornell_box_smoke},
                                               {3, final_scene}};
  const auto it = funcs.find(i);
  if (it == funcs.cend())
    return std::nullopt;
  return it->second();
}

int main(int argc, char const *argv[]) {
  const auto data_opt = get_scene(1);
  if (!data_opt) {
    std::cout << "Invalid scene selected" << std::endl;
    return -1;
  }
  const auto data = *data_opt;
  const auto img_h = static_cast<int>(data.img_w / data.aspect_ratio);

  const Tracer::Camera camera(data.camera_origin, data.camera_lookat, data.up,
                              data.aspect_ratio, data.vfov, data.aperture,
                              data.focal_distance, data.start_time,
                              data.end_time);

  const auto p = std::filesystem::current_path() / "image.ppm";
  std::ofstream out;
  out.open(p, std::ios::trunc);
  out << "P3\n" << data.img_w << ' ' << img_h << "\n255\n";

  auto thread_n = std::thread::hardware_concurrency();
  thread_n = thread_n ? thread_n : 2;
  const int N = img_h;
  const int k = img_h / N;
  const int r = img_h % N;

  boost::asio::thread_pool pool(thread_n);
  const auto pixel_n = img_h * data.img_w;
  std::vector<Util::RGB<uint8_t>> image(pixel_n);

  auto rem = r;

  for (int i = 0; i < N; ++i) {
    const int h = img_h - i * k - (r - rem);
    const int l = img_h - (i + 1) * k - (rem == 0 ? 0 : 1);
    auto b = image.begin() + (i * k + (r - rem)) * data.img_w;
    rem = rem == 0 ? 0 : rem - 1;

    const auto f = [&data, &camera, img_h, h, l, b]() mutable {
      const auto samples_per_pixel_inv = 1. / data.samples_per_pixel;
      auto i = h - 1;
      for (; i >= l; --i) {
        for (auto j = 0; j < data.img_w; ++j) {
          Util::RGB<double> rgb_acc{0., 0., 0.};
          for (auto k = 0; k < data.samples_per_pixel; ++k) {
            const auto u = static_cast<double>(j + Util::rando_double()) /
                           (data.img_w - 1),
                       v = static_cast<double>(i + Util::rando_double()) /
                           (img_h - 1);
            const auto ray = camera.yeet_ray(u, v);
            const auto rgb =
                trace(data.scene, ray, data.bg_clr, data.recursion_depth);
            rgb_acc += rgb;
          }
          rgb_acc *= samples_per_pixel_inv;
          *(b++) = Util::convert_rgb(
              Util::clamp_rgb(rgb_acc, 0., 1.)
                  .gamma_correct([](const double p) { return sqrt(p); }),
              static_cast<uint8_t>(255));
        }
        ++lines_complete;
      }
    };
    boost::asio::post(pool, f);
  }

  int lines = -1, lines_temp;
  while ((lines_temp = lines_complete) < img_h)
    if (lines != lines_temp) {
      lines = lines_temp;
      std::cout << std::setprecision(5);
      std::cout << "\rProgress: " << std::setw(5) << std::right
                << lines * 100. / img_h << "% " << std::flush;
      std::this_thread::sleep_for(std::chrono::seconds{1});
    }
  pool.join();
  std::cout << "\rProgress: 100% " << std::endl;

  std::cout << "Writing to file" << std::endl;
  for (const auto &rgb : image) {
    out << static_cast<int>(rgb.r()) << ' ' << static_cast<int>(rgb.g()) << ' '
        << static_cast<int>(rgb.b()) << '\n';
  }
  return 0;
}

Util::RGB<double> trace(const Tracer::Traceable &scene, const Tracer::Ray &ray,
                        const Util::RGB<double> bg_colour, int depth) {
  if (depth == 0)
    return Util::black;
  // t_min is 0.001 to fix issues with shadow acne
  const auto cm = scene.contact(ray, 0.001, Util::infinity);
  if (!cm.has_value())
    return bg_colour;
  const auto sr = cm->material->scatter(ray, cm->record);
  if (!sr.scatter.has_value())
    return sr.emitted;
  return sr.emitted + sr.scatter->attenuation *
                          trace(scene, sr.scatter->ray, bg_colour, depth - 1);
}

Util::RGB<double> random_colour() {
  return {Util::rando_double(), Util::rando_double(), Util::rando_double()};
}

Util::RGB<double> random_colour(double min, double max) {
  return {Util::rando_double(min, max), Util::rando_double(min, max),
          Util::rando_double(min, max)};
}

Scene_Data random_scene() {
  using namespace Tracer;
  using namespace Util;
  using namespace Materials;
  using namespace Geometry;
  using namespace Textures;
  Traceable_List scene;
  auto checker =
      std::make_shared<Checker>(make_rgb(1., 0., 0.), make_rgb(1., 1., 1.), 5.);
  auto ground_material = std::make_shared<Lambertian>(checker);
  scene.insert(std::make_shared<Sphere>(0., -1000, 0., 1000., ground_material));
  const auto white_light = std::make_shared<Emissive>(make_rgb(2., 2., 2.));
  std::shared_ptr<Material> material;
  for (int a = -11; a < 11; a++) {
    for (int b = -11; b < 11; b++) {
      auto choose_mat = rando_double();
      Point3 centre{a + 0.9 * rando_double(), 0.2, b + 0.9 * rando_double()};
      if (Vec3(centre - Point3{4., 0.2, 0.}).modulus() > 0.9) {
        if (choose_mat < 0.8) {
          auto albedo = random_colour() * random_colour();
          material = std::make_shared<Lambertian>(albedo);
          if (choose_mat < 0.4) {
            scene.insert(std::make_shared<Sphere>(centre, 0.2, material));
          } else {
            scene.insert(std::make_shared<Box>(centre - Point3{0.2, 0.2, 0.2},
                                               centre + Point3{0.2, 0.2, 0.2},
                                               material));
          }
        } else if (choose_mat < 0.95) {
          auto albedo = random_colour(0.5, 1.);
          auto fuzz = rando_double(0., 0.5);
          material = std::make_shared<Metal>(albedo, fuzz);
          scene.insert(std::make_shared<Sphere>(centre, 0.2, material));
        } else {
          material = std::make_shared<Glass>(1.5);
          scene.insert(std::make_shared<Sphere>(centre, 0.2, material));
        }
      }
    }
  }

  auto material1 = std::make_shared<Glass>(1.5);
  scene.insert(std::make_shared<Sphere>(0., 1., 0., 1., material1));

  auto material2 = std::make_shared<Lambertian>(make_rgb(0.4, 0.2, 0.1));
  scene.insert(std::make_shared<Sphere>(-4., 1., 0., 1., material2));

  auto material3 = std::make_shared<Metal>(make_rgb(0.7, 0.6, 0.5), 0.0);
  scene.insert(std::make_shared<Sphere>(4., 1., 0., 1., material3));

  Scene_Data data;
  data.camera_origin = {13, 2., 3.};
  data.camera_lookat = {0., 0., 0.};
  data.up = {0., 1., 0.};
  data.vfov = 20.;
  data.aperture = 0.1;
  data.focal_distance = 10.;
  data.samples_per_pixel = 5000;
  data.recursion_depth = 50;
  data.bg_clr = {0.70, 0.80, 1.};
  data.img_w = 1200;
  data.aspect_ratio = 3. / 2.;
  data.scene = BVH(scene, 0., 0.);
  data.start_time = 0.;
  data.end_time = 0.;
  return data;
}

Scene_Data cornell_box() {
  using namespace Textures;
  using namespace Geometry;
  using namespace Materials;
  using namespace Util;
  Tracer::Traceable_List scene;
  const auto red =
      std::make_shared<Lambertian>(Util::make_rgb(0.65, 0.05, 0.05));
  const auto white =
      std::make_shared<Lambertian>(Util::make_rgb(0.73, 0.73, 0.73));
  const auto green =
      std::make_shared<Lambertian>(Util::make_rgb(0.12, 0.45, 0.15));
  const auto light = std::make_shared<Materials::Emissive>(
      std::make_shared<Textures::Solid_Colour>(1., 1., 1.));

  scene.insert(std::make_shared<YZ_Rectangle>(0., 555., 0., 555., 555., green));
  scene.insert(std::make_shared<YZ_Rectangle>(0., 555., 0., 555., 0, red));
  scene.insert(std::make_shared<XZ_Rectangle>(0., 555., 0., 555., 555., light));
  scene.insert(std::make_shared<XZ_Rectangle>(0., 555., 0., 555., 0., white));
  // scene.insert(std::make_shared<XZ_Rectangle>(0., 555., 0., 555., 555.,
  // white));
  scene.insert(std::make_shared<XY_Rectangle>(0., 555., 0., 555., 555., white));

  Mat4 transform = Mat4::make_translation_matrix(265., 0., 295.) *
                   Mat4::make_rotation_matrix<Axis::Y>(15.);
  auto b =
      std::make_shared<Box>(Point3{0., 0., 0.}, Point3{165., 330, 165}, white);
  scene.insert(std::make_shared<Geometry::Transform>(std::move(b), transform));
  b = std::make_shared<Box>(Point3{0., 0., 0.}, Point3{165., 165, 165}, white);
  transform = Mat4::make_translation_matrix(130., 0., 65.) *
              Mat4::make_rotation_matrix<Axis::Y>(-18.);
  scene.insert(std::make_shared<Geometry::Transform>(std::move(b), transform));

  Scene_Data data;
  data.camera_origin = {278., 278., -800.};
  data.camera_lookat = {278., 278., 0.};
  data.up = {0., 1., 0.};
  data.vfov = 40.;
  data.aperture = 0.;
  data.focal_distance = 10.;
  data.samples_per_pixel = 500;
  data.recursion_depth = 50;
  data.bg_clr = {0., 0., 0.};
  data.img_w = 600;
  data.aspect_ratio = 1.;
  data.scene = Tracer::BVH(scene, 0., 0.);
  data.start_time = 0.;
  data.end_time = 0.;
  return data;
}

Scene_Data cornell_box_smoke() {
  using namespace Textures;
  using namespace Geometry;
  using namespace Materials;
  using namespace Util;
  using namespace Tracer;
  Tracer::Traceable_List scene;
  const auto red =
      std::make_shared<Lambertian>(Util::make_rgb(0.65, 0.05, 0.05));
  const auto white =
      std::make_shared<Lambertian>(Util::make_rgb(0.73, 0.73, 0.73));
  const auto green =
      std::make_shared<Lambertian>(Util::make_rgb(0.12, 0.45, 0.15));
  const auto light = std::make_shared<Materials::Emissive>(
      std::make_shared<Textures::Solid_Colour>(7., 7., 7.));

  scene.insert(std::make_shared<YZ_Rectangle>(0., 555., 0., 555., 555., green));
  scene.insert(std::make_shared<YZ_Rectangle>(0., 555., 0., 555., 0, red));
  scene.insert(
      std::make_shared<XZ_Rectangle>(113., 443., 127., 432., 554., light));
  scene.insert(std::make_shared<XZ_Rectangle>(0., 555., 0., 555., 0., white));
  scene.insert(std::make_shared<XZ_Rectangle>(0., 555., 0., 555., 555., white));
  scene.insert(std::make_shared<XY_Rectangle>(0., 555., 0., 555., 555., white));

  Mat4 transform = Mat4::make_translation_matrix(265., 0., 295.) *
                   Mat4::make_rotation_matrix<Axis::Y>(15.);
  auto b = Transform{
      std::make_shared<Box>(Point3{0., 0., 0.}, Point3{165., 330, 165}, white),
      transform};
  scene.insert(std::make_shared<Constant_Medium<Transform>>(std::move(b), 0.01,
                                                            Util::black));
  transform = Mat4::make_translation_matrix(130., 0., 65.) *
              Mat4::make_rotation_matrix<Axis::Y>(-18.);
  auto b1 = Transform(
      std::make_shared<Box>(Point3{0., 0., 0.}, Point3{165., 165, 165}, white),
      transform);
  scene.insert(std::make_shared<Constant_Medium<Transform>>(
      std::move(b1), 0.01, make_rgb(1., 1., 1.)));

  Scene_Data data;
  data.camera_origin = {278., 278., -800.};
  data.camera_lookat = {278., 278., 0.};
  data.up = {0., 1., 0.};
  data.vfov = 40.;
  data.aperture = 0.;
  data.focal_distance = 10.;
  data.samples_per_pixel = 500;
  data.recursion_depth = 50;
  data.bg_clr = {0., 0., 0.};
  data.img_w = 600;
  data.aspect_ratio = 1.;
  data.scene = Tracer::BVH(scene, 0., 0.);
  data.start_time = 0.;
  data.end_time = 0.;
  return data;
}

Scene_Data final_scene() {
  using namespace Textures;
  using namespace Geometry;
  using namespace Materials;
  using namespace Util;
  using namespace Tracer;
  using std::make_shared;
  Tracer::Traceable_List scene;

  // Square light
  const auto light = make_shared<Emissive>(make_rgb(7., 7., 7.));
  scene.insert(make_shared<XZ_Rectangle>(123., 423., 147., 412., 554., light));

  // Ground boxes
  const auto ground = make_shared<Lambertian>(make_rgb(0.9098, 0.5607, 0.1020));
  const auto boxes_per_side = 20;
  for (int i = 0; i < boxes_per_side; ++i) {
    for (int j = 0; j < boxes_per_side; ++j) {
      const auto w = 100.0;
      const auto x0 = -1000. + i * w;
      const auto z0 = -1000. + j * w;
      const auto y0 = 0.;
      const auto x1 = x0 + w;
      const auto y1 = rando_double(1., 101.);
      const auto z1 = z0 + w;
      scene.insert(
          make_shared<Box>(Point3{x0, y0, z0}, Point3{x1, y1, z1}, ground));
    }
  }

  // Moving sphere
  const auto centre1 = Point3{400., 400., 200.};
  const auto centre2 = Point3{30., 0., 0.} + centre1;
  const auto moving_sphere_material =
      make_shared<Lambertian>(make_rgb(0.48, 0.83, 0.53));
  auto moving_sphere = make_shared<Moving_Sphere>(centre1, 0., centre2, 1., 50.,
                                                  moving_sphere_material);
  scene.insert(std::move(moving_sphere));

  // Glass sphere
  const auto glass = make_shared<Glass>(1.5);
  scene.insert(make_shared<Sphere>(Point3{260., 150., 45.}, 50., glass));

  // Dull metal sphere
  scene.insert(
      make_shared<Sphere>(Point3{0., 150., 145.}, 50.,
                          make_shared<Metal>(make_rgb(0.8, 0.8, 0.9), 1.)));

  // Shiny metal sphere
  scene.insert(
      make_shared<Sphere>(Point3{220., 280., 300.}, 80.,
                          make_shared<Metal>(make_rgb(0.7, 0.7, 0.8), 0.1)));

  // Earth
  const auto emat =
      make_shared<Lambertian>(make_shared<Image>("../images/earthmap.jpg"));
  scene.insert(make_shared<Sphere>(Point3{400., 200., 400.}, 100., emat));

  // Blue sphere
  scene.insert(make_shared<Sphere>(360., 150., 145., 70., glass));
  scene.insert(make_shared<Constant_Medium<Sphere>>(
      0.2, make_rgb(0.2, 0.4, 0.9), 360., 150., 145., 70., glass));

  // Atmosphere
  // auto fog = make_shared<Constant_Medium<Sphere>>(
  //     Sphere{0., 0., 0., 5000., glass}, 0.00005, make_rgb(1., 1., 1.));
  // scene.insert(std::move(fog));

  // Sphere cluster
  const auto white = make_shared<Lambertian>(make_rgb(0.73, 0.73, 0.73));
  const auto ns = 1000;
  const auto transform = Mat4::make_translation_matrix(-100., 270., 395.) *
                         Mat4::make_rotation_matrix<Axis::Y>(15.);
  auto mtx = std::make_shared<Geometry::Trans_Mtx>(transform);
  for (int j = 0; j < ns; ++j) {
    auto s = std::make_shared<Sphere>(Point3{rando_double(0., 165.),
                                             rando_double(0., 165.),
                                             rando_double(0., 165.)},
                                      10., white);
    scene.insert(make_shared<Transform>(std::move(s), transform));
  }

  Scene_Data data;
  data.camera_origin = {478., 278., -600.};
  data.camera_lookat = {278., 278., 0.};
  data.up = {0., 1., 0.};
  data.vfov = 40.;
  data.aperture = 0.;
  data.focal_distance = 10.;
  data.samples_per_pixel = 10000;
  data.recursion_depth = 50;
  data.bg_clr = {0., 0., 0.};
  data.img_w = 800;
  data.aspect_ratio = 1.;
  data.scene = Tracer::BVH(scene, 0., 1.);
  data.start_time = 0.;
  data.end_time = 1.;
  return data;
}