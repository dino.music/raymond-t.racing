#include "Textures/image.hpp"
#include "Textures/stb_image.h"
#include <stdexcept>

namespace Textures {

Image::Image(std::string_view file_name) {
  auto bytes_per_pixel = 3;

  auto ptr =
      stbi_load(file_name.data(), &w, &h, &bytes_per_pixel, bytes_per_pixel);
  if (!ptr)
    throw std::invalid_argument{std::string("File: ") + std::string(file_name) +
                                " doesn't exist"};
  data = std::unique_ptr<unsigned char[]>(ptr);
  bytes_per_scanline = w * 3;
}

Util::RGB<double> Image::attenuation(double u, double v,
                                     const Geometry::Point3 &p) const {
  v = 1. - v;
  const auto i = static_cast<int>((w - 1) * u);
  const auto j = static_cast<int>((h - 1) * v);
  const auto px = bytes_per_scanline * j + 3 * i;
  const auto rgb = Util::make_rgb(data[px], data[px + 1], data[px + 2]);
  return Util::convert_rgb(rgb, 1. / 255.);
}

} // namespace Textures
