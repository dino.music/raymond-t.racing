#include "Util/random.hpp"
#include "Util/constants.hpp"
#include <math.h>

namespace Util {

thread_local std::default_random_engine random_engine{5};
thread_local std::uniform_real_distribution<double> unit_sphere_dist{-1., 1.};
thread_local std::uniform_real_distribution<double> dist{0, 1.};

Geometry::Vec3 random_vector_on_unit_sphere() {
  const auto r1 = rando_double(), r2 = rando_double();
  const auto phi = 2 * Util::pi * r1;
  const auto cos_theta = 1. - 2 * r2, sin_theta = 2 * sqrt(r2 * (1. - r2));
  const auto sin_phi = std::sin(phi), cos_phi = std::cos(phi);
  return {cos_phi * sin_theta, sin_phi * sin_theta, cos_theta};
}

Geometry::Vec3 random_vector_in_unit_circle() {
  // Uniformly distributed in [-1, 1]
  const auto x = rando_double() * 2. - 1., y = rando_double() * 2. - 1.;
  double r, phi;
  if (x > -y) {
    if (x > y) { // First quadrant
      r = x;
      phi = y / x;
    } else { // Second quadrant
      r = y;
      phi = 2 - x / y;
    }
  } else {
    if (x < y) { // Third quadrant
      r = -x;
      phi = 4 + y / x;
    } else { // Fourth quadrant
      r = -y;
      phi = y != 0 ? 6 - x / y : 0.;
    }
  }
  phi *= Util::pi / 4.;
  return {r * std::cos(phi), r * std::sin(phi), 0.};
}

Geometry::Vec3 random_vector_on_hemisphere(const double e) {
  const auto r1 = rando_double(), r2 = rando_double();
  const auto phi = 2 * Util::pi * r1;
  const auto cos_phi = std::cos(phi), sin_phi = std::sin(phi);
  const auto cos_theta = std::pow(1. - r2, 1. / (1. + e)),
             sin_theta = sqrt(1. - cos_theta * cos_theta);
  return {sin_theta * cos_phi, sin_theta * sin_phi, cos_theta};
}

double rando_double() { return dist(random_engine); }

double rando_double(double min, double max) {
  return min + (max - min) * rando_double();
}

Geometry::Vec3 random_vector_in_unit_sphere() {
  Geometry::Vec3 rand;
  do {
    rand = Geometry::Vec3{unit_sphere_dist(random_engine),
                          unit_sphere_dist(random_engine),
                          unit_sphere_dist(random_engine)};
  } while (rand.modulus_squared() > 1);
  return rand;
}

} // namespace Util
