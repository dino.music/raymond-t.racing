#include "Geometry/mat4.hpp"

namespace Geometry {

Mat4 Mat4::operator*(const Mat4 &o) const {
  Mat4 temp;
  for (int i = 0; i < 4; ++i) {
    for (int j = 0; j < 4; ++j) {
      double acc = 0.;
      for (int k = 0; k < 4; ++k) {
        acc += coeff[i][k] * o.coeff[k][j];
      }
      temp.coeff[i][j] = acc;
    }
  }
  return temp;
}

std::optional<Mat4> Mat4::inverse() const {
  Mat4 temp;
  if (invert_matrix(reinterpret_cast<const double *>(coeff.data()),
                    reinterpret_cast<double *>(temp.coeff.data())))
    return temp;
  return std::nullopt;
}

Mat4 Mat4::make_translation_matrix(double x, double y, double z) {
  Mat4 temp;
  temp.coeff[0][0] = 1.;
  temp.coeff[0][3] = x;
  temp.coeff[1][1] = 1.;
  temp.coeff[1][3] = y;
  temp.coeff[2][2] = 1.;
  temp.coeff[2][3] = z;
  temp.coeff[3][3] = 1.;
  return temp;
}

Point3 transform_point(const Mat4 &m, const Point3 &p) {
  const auto point = Geometry::expand_point(p, 1.);
  auto new_point = Geometry::Point3{0., 0., 0., 0.};
  for (int r = 0; r < 4; ++r) {
    for (int c = 0; c < 4; ++c) {
      new_point[r] += point[c] * m(r, c);
    }
  }
  return new_point;
}

Vec3 transform_vector(const Mat4 &m, const Vec3 &v) {
  auto new_vec = Geometry::Point3{0., 0., 0., 0.};
  for (int r = 0; r < 4; ++r) {
    for (int c = 0; c < 4; ++c) {
      new_vec[r] += v[c] * m(r, c);
    }
  }
  return new_vec;
}

Vec3 transform_normal(const Mat4 &m, const Vec3 &n) {
  auto new_normal = Geometry::Vec3{0., 0., 0., 0.};
  for (int c = 0; c < 4; ++c) {
    for (int r = 0; r < 4; ++r) {
      new_normal[c] += n[r] * m(r, c);
    }
  }
  return new_normal;
}

// this is lifted from opengl
bool Mat4::invert_matrix(const double m[16], double invOut[16]) const {
  double inv[16], det;
  int i;

  inv[0] = m[5] * m[10] * m[15] - m[5] * m[11] * m[14] - m[9] * m[6] * m[15] +
           m[9] * m[7] * m[14] + m[13] * m[6] * m[11] - m[13] * m[7] * m[10];

  inv[4] = -m[4] * m[10] * m[15] + m[4] * m[11] * m[14] + m[8] * m[6] * m[15] -
           m[8] * m[7] * m[14] - m[12] * m[6] * m[11] + m[12] * m[7] * m[10];

  inv[8] = m[4] * m[9] * m[15] - m[4] * m[11] * m[13] - m[8] * m[5] * m[15] +
           m[8] * m[7] * m[13] + m[12] * m[5] * m[11] - m[12] * m[7] * m[9];

  inv[12] = -m[4] * m[9] * m[14] + m[4] * m[10] * m[13] + m[8] * m[5] * m[14] -
            m[8] * m[6] * m[13] - m[12] * m[5] * m[10] + m[12] * m[6] * m[9];

  inv[1] = -m[1] * m[10] * m[15] + m[1] * m[11] * m[14] + m[9] * m[2] * m[15] -
           m[9] * m[3] * m[14] - m[13] * m[2] * m[11] + m[13] * m[3] * m[10];

  inv[5] = m[0] * m[10] * m[15] - m[0] * m[11] * m[14] - m[8] * m[2] * m[15] +
           m[8] * m[3] * m[14] + m[12] * m[2] * m[11] - m[12] * m[3] * m[10];

  inv[9] = -m[0] * m[9] * m[15] + m[0] * m[11] * m[13] + m[8] * m[1] * m[15] -
           m[8] * m[3] * m[13] - m[12] * m[1] * m[11] + m[12] * m[3] * m[9];

  inv[13] = m[0] * m[9] * m[14] - m[0] * m[10] * m[13] - m[8] * m[1] * m[14] +
            m[8] * m[2] * m[13] + m[12] * m[1] * m[10] - m[12] * m[2] * m[9];

  inv[2] = m[1] * m[6] * m[15] - m[1] * m[7] * m[14] - m[5] * m[2] * m[15] +
           m[5] * m[3] * m[14] + m[13] * m[2] * m[7] - m[13] * m[3] * m[6];

  inv[6] = -m[0] * m[6] * m[15] + m[0] * m[7] * m[14] + m[4] * m[2] * m[15] -
           m[4] * m[3] * m[14] - m[12] * m[2] * m[7] + m[12] * m[3] * m[6];

  inv[10] = m[0] * m[5] * m[15] - m[0] * m[7] * m[13] - m[4] * m[1] * m[15] +
            m[4] * m[3] * m[13] + m[12] * m[1] * m[7] - m[12] * m[3] * m[5];

  inv[14] = -m[0] * m[5] * m[14] + m[0] * m[6] * m[13] + m[4] * m[1] * m[14] -
            m[4] * m[2] * m[13] - m[12] * m[1] * m[6] + m[12] * m[2] * m[5];

  inv[3] = -m[1] * m[6] * m[11] + m[1] * m[7] * m[10] + m[5] * m[2] * m[11] -
           m[5] * m[3] * m[10] - m[9] * m[2] * m[7] + m[9] * m[3] * m[6];

  inv[7] = m[0] * m[6] * m[11] - m[0] * m[7] * m[10] - m[4] * m[2] * m[11] +
           m[4] * m[3] * m[10] + m[8] * m[2] * m[7] - m[8] * m[3] * m[6];

  inv[11] = -m[0] * m[5] * m[11] + m[0] * m[7] * m[9] + m[4] * m[1] * m[11] -
            m[4] * m[3] * m[9] - m[8] * m[1] * m[7] + m[8] * m[3] * m[5];

  inv[15] = m[0] * m[5] * m[10] - m[0] * m[6] * m[9] - m[4] * m[1] * m[10] +
            m[4] * m[2] * m[9] + m[8] * m[1] * m[6] - m[8] * m[2] * m[5];

  det = m[0] * inv[0] + m[1] * inv[4] + m[2] * inv[8] + m[3] * inv[12];

  if (det == 0)
    return false;

  det = 1.0 / det;

  for (i = 0; i < 16; i++)
    invOut[i] = inv[i] * det;

  return true;
}

} // namespace Geometry
