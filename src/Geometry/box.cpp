#include "Geometry/box.hpp"

namespace Geometry {

std::optional<Tracer::Contact_Material>
Box::contact(const Tracer::Ray &ray, double t_min, double t_max) const {
  using boost::math::sign;
  for (int i = 0; i < 3; ++i) {
    const auto d = ray.inv_direction()[i];
    auto t0 = (lower[i] - ray.origin()[i]) * d;
    auto t1 = (upper[i] - ray.origin()[i]) * d;
    if (d < 0.)
      std::swap(t0, t1);
    t_min = std::max(t0, t_min);
    t_max = std::min(t1, t_max);
    if (t_max <= t_min)
      return std::nullopt;
  }
  const auto t = t_min < 0 ? t_max : t_min;
  const auto t1 = t_min < 0 ? t_min : t_max;
  const auto contact_point = ray(t);
  const auto p = contact_point - centre;
  auto normal = Geometry::Vec3{static_cast<int>(p.x() * invRadius.x() * 1.01),
                               static_cast<int>(p.y() * invRadius.y() * 1.01),
                               static_cast<int>(p.z() * invRadius.z() * 1.01)};
  if (normal.epsilon_zero(0.00001))
    return std::nullopt;
  normal = normal.normalize();
  const auto texturec = Geometry::Vec3(contact_point - lower) * invRadius * 0.5;
  // This isn't correct
  const auto [ut, vt] =
      normal.x() ? std::pair{texturec.y(), texturec.z()}
                 : normal.y() ? std::pair{texturec.x(), texturec.z()}
                              : std::pair{texturec.x(), texturec.y()};
  return Tracer::Contact_Material{{contact_point, normal, t, ut, vt, t1},
                                  material};
}

} // namespace Geometry
