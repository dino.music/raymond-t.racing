#include "Geometry/transform.hpp"

namespace Geometry {

std::optional<Tracer::Contact_Material>
Transform::contact(const Tracer::Ray &ray, double t_min, double t_max) const {
  // Direction should be expanded with zero, because we only want to rotate
  // it, never to translate Origin should be expanded with one
  const auto new_origin =
      Geometry::transform_point(mtx->inv_trans, ray.origin());
  const auto new_dir =
      Geometry::transform_vector(mtx->inv_trans, ray.direction());
  Tracer::Ray moved_r{new_origin, new_dir, ray.time()};
  auto cr = obj->contact(moved_r, t_min, t_max);
  if (!cr.has_value())
    return std::nullopt;

  // Contact point should be expanded with one
  // Normal should be expanded with zero
  const auto cp =
      Geometry::transform_point(mtx->trans, cr->record.contact_point);
  const auto n =
      Geometry::transform_normal(mtx->trans, cr->record.surface_normal);
  return Tracer::Contact_Material{{cp, n, cr->record.t, cr->record.texture_u,
                                   cr->record.texture_v, cr->record.t1},
                                  std::move(cr->material)};
}

std::optional<Tracer::Bounding_Box> Transform::bounding_box(double t0,
                                                            double t1) const {
  const auto bb = obj->bounding_box(t0, t1);
  if (!bb.has_value())
    return std::nullopt;
  const auto lower = Geometry::expand_point(bb->lower(), 1.);
  const auto upper = Geometry::expand_point(bb->upper(), 1.);
  Geometry::Point3 l{0., 0., 0., 0.}, u{0., 0., 0., 0.};
  for (int r = 0; r < 4; ++r) {
    for (int c = 0; c < 4; ++c) {
      auto a = mtx->trans(r, c) * lower[c];
      auto b = mtx->trans(r, c) * upper[c];
      if (a > b)
        std::swap(a, b);
      l[r] += a;
      u[r] += b;
    }
  }
  return Tracer::Bounding_Box{l, u};
}

} // namespace Geometry
