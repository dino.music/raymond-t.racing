#include "Geometry/moving_sphere.hpp"
#include "Util/constants.hpp"

namespace Geometry {

Geometry::Point3 Moving_Sphere::centre(double t) const {
  return cs + (cd - cs) * ((t - ts) / (td - ts));
}

std::optional<Tracer::Contact_Material>
Moving_Sphere::contact(const Tracer::Ray &ray, double t_min,
                       double t_max) const {
  const auto centre_current = centre(ray.time());
  const auto ac = Geometry::Vec3(ray.origin() - centre_current);
  const auto a = ray.direction().modulus_squared();
  const auto b_half = ray.direction().dot_prod(ac);
  const auto c = ac.modulus_squared() - r * r;
  const auto D = b_half * b_half - a * c;

  if (D < 0)
    return {};

  const auto sqrtd = sqrt(D);
  auto r = -(b_half + sqrtd) / a;
  if (r < t_min || r > t_max) {
    r = (-b_half + sqrtd) / a;
    if (r < t_min || r > t_max)
      return std::nullopt;
  }

  const auto contact_point = ray(r);
  auto surface_normal = (contact_point - centre_current) * (1 / r);
  const auto [u, v] = texture_coords(surface_normal);
  return Tracer::Contact_Material{{contact_point, surface_normal, r, u, v},
                                  material};
}

std::optional<Tracer::Bounding_Box>
Moving_Sphere::bounding_box(double t0, double t1) const {
  const auto rp = Geometry::Point3{r, r, r};
  const auto c0 = centre(t0), c1 = centre(t1);
  const auto boxl = Tracer::Bounding_Box{c0 - rp, c0 + rp};
  const auto boxu = Tracer::Bounding_Box{c1 - rp, c1 + rp};
  return boxl.encompassing_box(boxu);
}

std::pair<double, double>
Moving_Sphere::texture_coords(const Geometry::Point3 &p) const {
  const auto phi = atan2(-p.z(), p.x()) + Util::pi;
  const auto theta = acos(-p.y());
  return {phi / (2 * Util::pi), theta / Util::pi};
}

} // namespace Geometry
