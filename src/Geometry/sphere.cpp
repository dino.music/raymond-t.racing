#include "Geometry/sphere.hpp"
#include "Util/constants.hpp"
#include <cmath>

namespace Geometry {

// Generalized sphere equation (x - Cx)^2 + (x - Cy)^2 + (x - Cz)^2 = r^2
// Where (Cx, Cy, Cy)  = C is the centre of the sphere
// A vector from the centre of the sphere to a point on the surface is given by
// P - C, where P = (x, y, z)
// (P - C) * (P - C) (dot product) gives (x - Cx)^2 + (x - Cy)^2 + (x - Cz)^2
// If we are looking for the point of contact of our ray and sphere, we can plug
// in the ray's P(t) for P, giving (P(t) - C) * (P(t) - C) = r^2
// We're looking for a t that satisfies the condition
// Expanding with P(t) = A + bt we get
// (A + bt - C) * (A + bt - C) = r^2
// ||A|| + (b*A)t - C*A + (A*b)t + ||b||t^2 - (C*b)t - A*C - (b*C)t + ||C|| =
// r^2
// ||b||^2t^2 + 2tb*[(A - C)] + -2(A*C) + ||A|| + ||C|| = r ^ 2
// ||b||^2t^2 + 2tb[(A - C)] + ||(A - C)||^2 - r^2 = 0
// which gives us a basic quadratic equation
std::optional<Tracer::Contact_Material>
Sphere::contact(const Tracer::Ray &ray, double t_min, double t_max) const {
  const auto ac = Geometry::Vec3(ray.origin() - centre);
  const auto a = ray.direction().modulus_squared();
  const auto b_half = ray.direction().dot_prod(ac);
  const auto c = ac.modulus_squared() - radius * radius;
  const auto D = b_half * b_half - a * c;

  if (D < 0)
    return {};

  const auto sqrtd = sqrt(D);
  const auto r1 = -(b_half + sqrtd) / a;
  const auto r2 = (-b_half + sqrtd) / a;
  double t0, t1;
  if (r1 >= t_min && r1 <= t_max) {
    t0 = r1;
    t1 = r2;
  } else if (r2 >= t_min && r2 <= t_max) {
    t0 = r2;
    t1 = r1;
  } else
    return std::nullopt;

  const auto contact_point = ray(t0);
  // Normal always points from the centre outward. We will need to determine
  // where the ray came from at coloring time. If the dot product of ray
  // direction and surface normal is positive, the ray came from inside. The
  // opposite is true for a negative dot product
  auto surface_normal = (contact_point - centre) * (1 / radius);
  const auto [u, v] = texture_coords(surface_normal);
  return Tracer::Contact_Material{{contact_point, surface_normal, t0, u, v, t1},
                                  material};
}

std::optional<Tracer::Bounding_Box> Sphere::bounding_box(double, double) const {
  const auto rp = Geometry::Point3{radius, radius, radius};
  return Tracer::Bounding_Box{centre - rp, centre + rp};
}

std::pair<double, double>
Sphere::texture_coords(const Geometry::Point3 &p) const {
  // Theta - angle from bottom pole( from -Y)
  // Phi - angle around the Y-axis(from -X to Z to X to -Z)
  // y = -cos(theta)
  // x =  -sin(theta)*cos(phi)
  // z = sin(theta)*sin(phi)
  // Phi =  atan2(z, -x)
  // atan2 returns values from 0 to pi, and then from -pi to 0
  // atan2(-y, -x) + pi will yield a value betwenn 0 and 2pi
  const auto phi = atan2(-p.z(), p.x()) + Util::pi;
  const auto theta = acos(-p.y());
  return {phi / (2 * Util::pi), theta / Util::pi};
}

} // namespace Geometry
